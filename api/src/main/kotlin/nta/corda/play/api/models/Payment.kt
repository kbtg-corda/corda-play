package nta.corda.play.api.models

import com.fasterxml.jackson.annotation.JsonFormat
import net.corda.core.contracts.Amount
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import nta.corda.play.payment.contracts.PaymentContract
import nta.corda.play.payment.models.Purpose
import java.time.Instant
import java.util.*

data class PaymentRequest(
        val receiver: Party,
        val amount: Amount<Currency>,

        val effectiveDate: Instant = Instant.now(),

        val issueDate: Instant? = null,

        val purpose: Purpose,
        val remark: String = ""
)

data class PaymentResponse(
        val txId: String,
        val receiver: Party? = null,
        val amount: Amount<Currency>,
        val status: PaymentContract.Status,
        val purpose: Purpose,
        val effectiveDate: Instant = Instant.now(),
        val issueDate: Instant = Instant.now(),
        val remark: String? = null,
        val linearId: UniqueIdentifier = UniqueIdentifier()
)