package nta.corda.play.api.common

import net.corda.core.contracts.Amount
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

fun <T : Any> AMOUNT(amount: Double, token: T): Amount<T> = Amount.fromDecimal(BigDecimal.valueOf(amount), token, RoundingMode.HALF_EVEN)

@JvmField val THB: Currency = Currency.getInstance("THB")
@JvmField val HKD: Currency = Currency.getInstance("HKD")

fun String.toCurrency(): Currency = when (this.toUpperCase()) {
    "HKD" -> HKD
    "THB" -> THB
    else -> throw IllegalArgumentException("Unsupported currency ${this.toUpperCase()}")
}