package nta.corda.play.api.models

import net.corda.core.contracts.Amount
import java.util.*

data class CashIssueRequest(
        val amount: Amount<Currency>
)