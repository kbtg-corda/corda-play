package nta.corda.play.api.controllers

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import nta.corda.play.api.serializers.DateTimeDeserializer
import nta.corda.play.api.serializers.DateTimeSerializer
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

data class Request(
    @JsonSerialize(using = DateTimeSerializer::class)
    @JsonDeserialize(using = DateTimeDeserializer::class)
    val issueDate: Instant? = null
)

@RestController
@RequestMapping
class Controller {

    @GetMapping()
    fun get() = object {
        val message: String = "Ok"
    }

//    @PostMapping("/deserialize/date")
//    fun create(@RequestBody request: Request): ResponseEntity<Any> {
//        return ResponseEntity.ok(object {
//            val message = "ok"
//        })
//    }
//
//    @GetMapping("/serialize/date")
//    fun get(): ResponseEntity<Any> {
//        return ResponseEntity.ok(Request(issueDate = LocalDateTime.now().toInstant(ZoneOffset.UTC)))
//    }
}
