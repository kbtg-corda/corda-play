package nta.corda.play.api.controllers

import net.corda.core.utilities.OpaqueBytes
import net.corda.core.utilities.getOrThrow
import net.corda.finance.flows.CashIssueFlow
import net.corda.finance.workflows.getCashBalance
import nta.corda.play.api.common.toCurrency
import nta.corda.play.api.models.CashIssueRequest
import nta.corda.play.api.services.CordappService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/cash/")
class CashController {
    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }

    private val proxy
        get() = CordappService.proxy

    @GetMapping("balance/{currency}")
    fun getBalance(@PathVariable(value = "currency") currency: String): ResponseEntity<Any> {
        val ccycode = currency.toCurrency()
        val balance = proxy.getCashBalance(ccycode)

        return ResponseEntity.ok(object {
            val quantity: Long = balance.quantity / 100
        })
    }

    @PostMapping("issue")
    fun issue(@RequestBody request: CashIssueRequest): ResponseEntity<Any> {

        val (amount) = request

        // 1. Prepare issue request.
        val notary = proxy.notaryIdentities().firstOrNull()
                ?: throw IllegalStateException("Could not find a notary.")

        val issueRef = OpaqueBytes.of(0)
        val issueRequest = CashIssueFlow.IssueRequest(
            amount =  amount,
            issueRef = issueRef,
            notary =  notary
        )

        // 2. Start flow and wait for response.
        val (status, message) = try {
            val flowHandle = proxy.startFlowDynamic(CashIssueFlow::class.java, issueRequest)
            val result = flowHandle.returnValue.getOrThrow()
            flowHandle.close()
            HttpStatus.CREATED to result.stx.tx.outputs.single().data
        } catch (e: Exception) {
            e.cause?.printStackTrace()
            e.printStackTrace()
            HttpStatus.BAD_REQUEST to e.message
        }

        // 3. Return the response.
        return ResponseEntity
            .status(status)
            .body(object {
                val amount = amount
            })
    }

}
