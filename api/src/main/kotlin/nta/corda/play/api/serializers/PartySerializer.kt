package nta.corda.play.api.serializers

import nta.corda.play.api.services.CordappService
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import org.slf4j.LoggerFactory
import org.springframework.boot.jackson.JsonComponent
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.context.support.SpringBeanAutowiringSupport

fun getPartyMap(party: Party) = mapOf(
        "name" to party.name.toString(),
        "displayName" to party.name.organisation
)

@JsonComponent
class PartySerializer : StdSerializer<Party>(Party::class.java) {
    override fun serialize(value: Party, gen: JsonGenerator, provider: SerializerProvider?) {
        gen.writeObject(getPartyMap(value))
    }
}

@JsonComponent
class PartyDeserializer : JsonDeserializer<Party>() {

    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }

    init {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this)
    }

    private val proxy
        get() = CordappService.proxy

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Party {
        logger.info("\n\ndeserialize: ${p.valueAsString}\n\n")

        val x500Name = CordaX500Name.parse(p.valueAsString)

        return proxy.wellKnownPartyFromX500Name(x500Name)
            ?: throw IllegalArgumentException("Receiver ${p.valueAsString} not found on network map.")
    }
}