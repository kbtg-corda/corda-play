package nta.corda.play.api.services

import net.corda.client.rpc.CordaRPCClient
import net.corda.client.rpc.CordaRPCConnection
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.utilities.NetworkHostAndPort
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.RestController
import javax.annotation.PreDestroy

object CordappService {
    private val logger = LoggerFactory.getLogger(RestController::class.java)

    val host: String by lazy {
        System.getenv("CORDAPP_HOST") ?: "localhost"
    }

    val username: String by lazy {
        System.getenv("CORDAPP_USER") ?: "user1"
    }

    val password: String by lazy {
        System.getenv("CORDAPP_PASS") ?: "password"
    }

    val port: Int by lazy {
        val port: String? = System.getenv("CORDAPP_PORT")

        logger.info("\n\nenv.rpcport: ${port}\n\n")

        if (port != null) {
            return@lazy port.toIntOrNull() ?: 10006
        }

        return@lazy 10006
    }

    lateinit var rpcConnection: CordaRPCConnection
        private set

    val proxy: CordaRPCOps by lazy {
        logger.info("\n\ndlt.uri: $username:$password@$host:$port\n\n")

//        val rpcAddress = NetworkHostAndPort(host, rpcPort)
        val addr = "$host:$port"

        val nodeHP = NetworkHostAndPort.parse(addr)
        val rpcClient = CordaRPCClient(nodeHP)
        val rpcConnection = rpcClient.start(username, password)

        rpcConnection.proxy
    }

    @PreDestroy
    fun close() {
        rpcConnection.notifyServerAndClose()
    }
}