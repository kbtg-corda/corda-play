package nta.corda.play.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import net.corda.core.contracts.Amount
import net.corda.core.identity.Party
import nta.corda.play.api.serializers.AmountDeserializer
import nta.corda.play.api.serializers.AmountSerializer
import nta.corda.play.api.serializers.PartyDeserializer
import nta.corda.play.api.serializers.PartySerializer
import org.springframework.boot.Banner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder


/**
 * A Spring Boot application.
 */

@Configuration
@SpringBootApplication
open class Server {
//    /**
//     * Spring Bean that binds a Corda Jackson object-mapper to HTTP message types used in Spring.
//     */
//    @Bean
//    open fun mappingJackson2HttpMessageConverter(@Autowired rpcConnection: CordappService): MappingJackson2HttpMessageConverter {
//        val mapper = JacksonSupport.createDefaultMapper(rpcConnection.proxy)
//        mapper.registerModule(JsonComponentModule())
//
//        val converter = MappingJackson2HttpMessageConverter()
//        converter.objectMapper = mapper
//        return converter
//    }

//    @Bean
//    open fun kotlinModule() = KotlinModule()

//    @Bean
//    open fun mappingJackson2HttpMessageConverter() = MappingJackson2HttpMessageConverter()
//        .apply {
//            this.objectMapper = ObjectMapper().apply {
//                registerModule(KotlinModule())
//            }
//        }

    @Bean
    open fun mapJsonConverter(): ObjectMapper {
        val simpleModule = SimpleModule()

        simpleModule.addSerializer(PartySerializer())
        simpleModule.addDeserializer(Party::class.java, PartyDeserializer())

        simpleModule.addSerializer(AmountSerializer())
        simpleModule.addDeserializer(Amount::class.java, AmountDeserializer())

//        simpleModule.addSerializer(DateTimeSerializer())
//        simpleModule.addDeserializer(Instant::class.java, DateTimeDeserializer())

        return Jackson2ObjectMapperBuilder
                .json()
                .modules(KotlinModule(), simpleModule)
                .build()
    }

}

object Application {
    @JvmStatic
    fun main(args: Array<String>) {
        val app = SpringApplication(Server::class.java)
        app.setBannerMode(Banner.Mode.OFF)
//        app.isWebEnvironment = true
        app.run(*args)
    }
}