package nta.corda.play.api.serializers

import nta.corda.play.api.common.AMOUNT
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import net.corda.core.contracts.Amount
import org.springframework.boot.jackson.JsonComponent
import java.util.*

@JsonComponent
class AmountSerializer : StdSerializer<Amount<*>>(Amount::class.java) {
    override fun serialize(value: Amount<*>, gen: JsonGenerator, provider: SerializerProvider?) {
        gen.writeStartObject()
        gen.writeStringField("value", value.toDecimal().toPlainString())
        gen.writeObjectField("token", value.token)
        gen.writeEndObject()
    }
}

@JsonComponent
class AmountDeserializer : StdDeserializer<Amount<*>>(Amount::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Amount<*> {
        if (p.currentToken != JsonToken.START_OBJECT) {
            val amountStr = p.valueAsString
            val match = "(\\d*\\.\\d+|\\d+)[\\s]*(\\w{3})".toRegex().matchEntire(amountStr.trim())
                    ?: throw JsonParseException(p, "Cannot parse $amountStr as Amount<Currency>.")
            val (_, value, token) = match.groupValues
            return AMOUNT(value.trim().toDouble(), Currency.getInstance(token.toUpperCase().trim()))
        }
        val node = p.codec.readTree<JsonNode>(p)
        val value = node.get("value").asDouble()
        @Suppress("unchecked_cast")
        val token = ctxt.readValue(node.get("token").traverse(), _valueClass.enclosingClass)
        return AMOUNT(value, token)
    }
}