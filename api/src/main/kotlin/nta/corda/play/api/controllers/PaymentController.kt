package nta.corda.play.api.controllers

import net.corda.core.flows.FlowException
import net.corda.core.messaging.startTrackedFlow
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.utilities.getOrThrow
import nta.corda.play.api.services.CordappService
import nta.corda.play.payment.flows.MakePayment
import nta.corda.play.payment.models.PaymentRequest
import nta.corda.play.payment.models.PaymentResponse
import nta.corda.play.payment.states.PaymentState
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/payment/")
class PaymentController {
    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }

    private val proxy
        get() = CordappService.proxy

    @PostMapping
    fun makePayment(@RequestBody request: PaymentRequest): ResponseEntity<Any> {

        return try {
            val response = proxy
                    .startTrackedFlow(MakePayment::Sender, request)
                    .returnValue
                    .getOrThrow()

            ResponseEntity
                .status(HttpStatus.CREATED)
                .body(response)
        }
        catch (ex: FlowException) {
            ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(object {
                    val message = ex.message!!
                })
        }
        catch (ex: Throwable) {
            ResponseEntity
                .badRequest()
                .body(object {
                    val message = ex.message!!
                })
        }
    }

    @GetMapping
    fun listPayments(): ResponseEntity<List<PaymentResponse>> {
        val payments = proxy
                .vaultQueryBy<PaymentState>()
                .states
    //                .filter { it.state.data.lender.equals(proxy.nodeInfo().legalIdentities.first()) }
                .map { it.state.data }
                .map {
                    PaymentResponse(
                        txId = it.txId,
    //                            receiver = ?
                        amount = it.amount,
                        status = it.status,
                        purpose = it.purpose,
                        effectiveDate = it.effectiveDate,
                        issueDate = it.issueDate,
                        remark = it.remark,
                        linearId = it.linearId
                    )
                }

        return ResponseEntity.ok(payments)
    }

}
