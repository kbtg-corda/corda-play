#!/bin/bash

cd ./api
sh gradlew clean build
cd ..

cd ./workflows
sh gradlew clean build
cd ..

jar='workflows-0.1.jar'
jarbld="./workflows/build/libs/$jar"
jarboa="./infra/nodes/bankofamerica/cordapps/$jar"
jardea="./infra/nodes/dealership/cordapps/$jar"
jarman="./infra/nodes/manufacturer/cordapps/$jar"
jarnot="./infra/nodes/notary/cordapps/$jar"

for f in [jarboa jardea jarman jarnot]; do
    if [ -f f ]; then
        rm f
    fi
    cp $jarbld f
done

# cp ./workflows/build/libs/workflows-0.1.jar ./infra/nodes/bankofamerica/cordapps/workflows-0.1.jar
# cp ./workflows/build/libs/workflows-0.1.jar ./infra/nodes/dealership/cordapps/workflows-0.1.jar
# cp ./workflows/build/libs/workflows-0.1.jar ./infra/nodes/manufacturer/cordapps/workflows-0.1.jar
# cp ./workflows/build/libs/workflows-0.1.jar ./infra/nodes/notary/cordapps/workflows-0.1.jar
