package nta.corda.play.payment.flows

import co.paralleluniverse.fibers.Suspendable
import net.corda.confidential.SwapIdentitiesFlow
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.CordaX500Name
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.minutes
import net.corda.finance.contracts.asset.PartyAndAmount
import net.corda.finance.workflows.asset.CashUtils
import net.corda.finance.workflows.getCashBalance
import nta.corda.play.common.txId
import nta.corda.play.payment.contracts.PaymentContract
import nta.corda.play.payment.models.PaymentRequest
import nta.corda.play.payment.models.PaymentResponse
import nta.corda.play.payment.states.PaymentState
import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object MakePayment {

    @InitiatingFlow
    @StartableByRPC
    class Sender(
        private val request: PaymentRequest
    ) : FlowLogic<PaymentResponse>() {

        /** The progress tracker provides checkpoints indicating the progress of the flow to observers. */
        override val progressTracker = ProgressTracker()

        /** The flow logic is encapsulated within the call() method. */
        @Suspendable
        override fun call() : PaymentResponse {

            val (
                receiver,
                amount,
                effectiveDate,
                issueDate,
                purpose,
                remark
            ) = request

//            0. verify cash sufficiency
            val cashBalance = serviceHub.getCashBalance(amount.token)
            logger.info("\n\nflow:payment:sender:0:cashBalance:${cashBalance}\n\n")

            if (cashBalance.quantity < amount.quantity) {
                throw FlowException("Insufficient cash (${amount.token}) to makes payment")
            }

//            1. specify notary
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            logger.info("\n\nflow:payment:sender:1\n\n")

//            2. create transaction (unsigned)
            val utx = TransactionBuilder(notary = notary)
            logger.info("\n\nflow:payment:sender:2\n\n")


//            3. create session
            val session = initiateFlow(receiver)

            logger.info("\n\nflow:payment:sender:3\n\n")



//            4. convert party to abstract party
            val identities = subFlow(SwapIdentitiesFlow(receiver))
            logger.info("\n\nflow:payment:sender:4.1:${identities}\n\n")

            val anonymousSender = identities[ourIdentity]
            val anonymousReceiver = identities[receiver]

            if (anonymousSender == null || anonymousReceiver == null) {
                throw FlowException("Identities mismatch!!")
            }

            logger.info("\n\nflow:payment:sender:4.2:${anonymousSender}:${anonymousReceiver}\n\n")

//            5. create output state
//            val operator = serviceHub.identityService.wellKnownPartyFromX500Name(operatorX500Name)
//                    ?: throw FlowException("Invalid operator!!")

            val outputState = PaymentState(
                txId = txId(
                    prefix = "payment",
                    name = serviceHub.myInfo.legalIdentities.first().name
                ),
                sender = anonymousSender!!,
                receiver = anonymousReceiver!!,
//                operator = operator!!,
                amount = amount,
                purpose = purpose,
                status = PaymentContract.Status.SETTLED,
                effectiveDate = effectiveDate,
                issueDate = issueDate ?: Instant.now(),
                remark = remark
            )

//            6. verify cash efficiency
//            TODO


//            7. add transaction output
            utx.addOutputState(outputState.copy(status = PaymentContract.Status.SETTLED))

            logger.info("\n\nflow:payment:sender:7\n\n")


//            8. add transaction command
            utx.addCommand(
                PaymentContract.Command.Pay(),
                listOf(
                    outputState.sender.owningKey,
                    outputState.receiver.owningKey
                )
            )

            logger.info("\n\nflow:payment:sender:8\n\n")



//            9. attach documents (ex. legal document)
//            TODO


//            10. set time window
            utx.setTimeWindow(serviceHub.clock.instant(), 5.minutes)
            logger.info("\n\nflow:payment:sender:10\n\n")


//            10.1 Get some cash from the vault and add a spend to our transaction builder
            val (_, cashSigningKeys) = CashUtils.generateSpend(serviceHub, utx, listOf(PartyAndAmount(receiver, amount)), ourIdentityAndCert)
            logger.info("\n\nflow:payment:sender:10.1\n\n")


//            11. Sign my transaction
            val myKeys = listOf(
                ourIdentity.owningKey,
                anonymousSender.owningKey
            )
            val ptx = serviceHub.signInitialTransaction(utx, cashSigningKeys + myKeys)
            logger.info("\n\nflow:payment:sender:11\n\n")


//            12. collect signatures (from parties)
            val stx = subFlow(CollectSignaturesFlow(ptx, listOf(session)))
            logger.info("\n\nflow:payment:sender:12\n\n")



            logger.info("\n\nflow:payment:sender:13-FinalityFlow:start\n\n")
//            13. send transaction to notary to verify of double spending
            subFlow(FinalityFlow(stx, session))

            logger.info("\n\nflow:payment:sender:13-FinalityFlow:end\n\n")

            val sar = stx.tx.outRefsOfType<PaymentState>().single()
            val state = sar.state.data

            return PaymentResponse(
                txId = state.txId,
                receiver = receiver,
                amount = amount,
                status = state.status,
                purpose = purpose,
                effectiveDate = effectiveDate,
                issueDate = issueDate!!,
                remark = remark,
                linearId = state.linearId
            )
        }
    }

    @InitiatedBy(Sender::class)
    class Responder(private val counterPartySession: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call() : SignedTransaction {
            logger.info("\n\nflow:payment:responder:1\n\n")

            val ptx = subFlow(object : SignTransactionFlow(counterPartySession) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    logger.info("\n\nflow:payment:responder:2\n\n")
                }
            })

            return subFlow(ReceiveFinalityFlow(counterPartySession, ptx.id))
        }
    }
}
