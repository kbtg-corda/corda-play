package nta.corda.play.payment.models

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import net.corda.core.contracts.Amount
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import nta.corda.play.common.DateTimeDeserializer
import nta.corda.play.common.DateTimeSerializer
import nta.corda.play.payment.contracts.PaymentContract
import java.time.Instant
import java.util.*

@CordaSerializable
enum class Purpose(val fullName: String) {
    SERVICE("Services"),
    INCOME("Incomes"),
    BORROW("Borrowing"),
    LEND("Lending"),
    GOODS("Goods"),
    OTHER("Others");

    companion object {
        private val purposeMap = values().associateBy { it.name }

        fun fromValue(code: String): Purpose? = purposeMap[code]
    }
}

@CordaSerializable
data class PaymentRequest(
    val receiver: Party,
    val amount: Amount<Currency>,

    @JsonSerialize(using = DateTimeSerializer::class)
    @JsonDeserialize(using = DateTimeDeserializer::class)
    val effectiveDate: Instant = Instant.now(),

    @JsonSerialize(using = DateTimeSerializer::class)
    @JsonDeserialize(using = DateTimeDeserializer::class)
    val issueDate: Instant? = null,

    val purpose: Purpose,
    val remark: String = ""
)

@CordaSerializable
data class PaymentResponse(
    val txId: String,
    val receiver: Party? = null,
    val amount: Amount<Currency>,
    val status: PaymentContract.Status,
    val purpose: Purpose,

    @JsonSerialize(using = DateTimeSerializer::class)
    @JsonDeserialize(using = DateTimeDeserializer::class)
    val effectiveDate: Instant = Instant.now(),

    @JsonSerialize(using = DateTimeSerializer::class)
    @JsonDeserialize(using = DateTimeDeserializer::class)
    val issueDate: Instant = Instant.now(),

    val remark: String? = null,
    val linearId: UniqueIdentifier = UniqueIdentifier()
)
