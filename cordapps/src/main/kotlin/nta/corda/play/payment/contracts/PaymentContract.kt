package nta.corda.play.payment.contracts

import net.corda.core.contracts.*
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.LedgerTransaction
import net.corda.finance.contracts.asset.Cash
import net.corda.finance.contracts.utils.sumCash
import nta.corda.play.payment.states.PaymentState
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class PaymentContract : Contract {
    companion object {
        val PAYMENT_CONTRACT_ID = PaymentContract::class.qualifiedName!!
        private val logger: Logger = LoggerFactory.getLogger(PaymentContract::class.java)
    }

    @CordaSerializable
    enum class Status {
//        QUEUED,
//        PENDING,
        SETTLED,
        CANCELED
    }

    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Command>()
        val inputs = tx.inRefsOfType<PaymentState>()
        val outputPaymentStates = tx.outputsOfType<PaymentState>()

        logger.info("\n\ncontract:payment:command:${command}\n\n")
        logger.info("\n\ncontract:payment:inputs:${inputs}\n\n")
        logger.info("\n\ncontract:payment:outputPaymentStates:${outputPaymentStates}\n\n")

        when (command.value) {
             is Command.Pay -> requireThat {

                 "Payment input state must be empty" using (inputs.isEmpty())
                 "Payment output state must be single" using (outputPaymentStates.size == 1)

                 val outputPaymentState = outputPaymentStates.first()
                 logger.info("\n\ncontract:payment:outputPaymentState:${outputPaymentState}\n\n")

                 "Payment output state's status must be settled" using (outputPaymentState.status == Status.SETTLED)

                 val inputCashStates = tx.inRefsOfType<Cash.State>()
                 logger.info("\n\ncontract:payment:inputCashStates:${inputCashStates}\n\n")

                 "Payment cash state must not be empty" using (inputCashStates.isNotEmpty())

                 val inputCashState = inputCashStates.map { it.state.data }
                 val cash = inputCashState.sumCash().quantity
                 val spendingAmount = outputPaymentState.amount.quantity

                 logger.info("\n\ncontract:payment:cash:${cash}\n\n")
                 logger.info("\n\ncontract:payment:spendingAmount:${spendingAmount}\n\n")

                 "The sender's cash must be greater or equal than sending amount" using (cash >= spendingAmount)
             }
        }
    }

    interface Command : CommandData {
        class Pay : TypeOnlyCommandData(), Command
//        class Queue : TypeOnlyCommandData(), Command
        class Cancel : TypeOnlyCommandData(), Command
//        class Pending : TypeOnlyCommandData(), Command
    }
}