package th.or.bot.inthanon.xborder.payment.contracts

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.Instant
import java.util.Currency
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Command
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.identity.AbstractParty
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.LedgerTransaction
import net.corda.finance.contracts.asset.Cash
import net.corda.finance.contracts.utils.sumCash
import net.corda.finance.contracts.utils.sumCashBy
import th.or.bot.inthanon.base.contract.Instruction
import th.or.bot.inthanon.base.contract.SettleableAgreement
import th.or.bot.inthanon.xborder.payment.models.Purpose

class XBorderPayment : Contract {
    companion object {
        val XBORDER_PAYMENT_CONTRACT_ID = XBorderPayment::class.qualifiedName!!
    }

    @CordaSerializable
    enum class Status {
        QUEUED,
        SETTLED,
        CANCELED
    }

    override fun verify(tx: LedgerTransaction) {
 //        TODO: verify input state
        val command = tx.commands.requireSingleCommand<Command>()

        when (command.value) {
            is Command.Pay -> requireThat {
                "The cross-border payment input states must be empty" using (tx.inRefsOfType<State>().isEmpty())

                val outPaymentStates = tx.outputsOfType<State>()
                val outInstructionStates = tx.outputsOfType<Instruction.State>()

                "The cross-border payment output state must be single" using (outPaymentStates.size == 1)
                "The instruction output state must be single" using (outInstructionStates.size == 1)

                val xborderPaymentState = outPaymentStates.first()
                val instructionState = outInstructionStates.first()

                "The cross-border payment output state's status must be settled" using (xborderPaymentState.status == Status.SETTLED)
                "The instruction output state's status must be settled" using (instructionState.status == Instruction.Status.SETTLED)

                val cashStates = tx.outputsOfType<Cash.State>()

                "The cash state's amount and cross-border payment output state's amount must be equal" using (cashStates.sumCashBy(xborderPaymentState.receiverBank).quantity == xborderPaymentState.sendingAmount.quantity)

                val cashInputStates = tx.inRefsOfType<Cash.State>().map { it.state.data }
                val cash = cashInputStates.sumCash().quantity
                val spendingAmount = xborderPaymentState.sendingAmount.quantity

                "The sender's cash must be greater or equal than sending amount" using (cash >= spendingAmount)
                val receiverCashOut = cashStates.filter { it.owner != xborderPaymentState.receiverBank }
                "The sender's cash remaining amount must be calculated properly" using  (
                    receiverCashOut.isEmpty() || receiverCashOut.sumCash().quantity == (cash - spendingAmount))
            }

            is Command.Queue -> requireThat {
                "The input states must be empty" using (tx.inRefsOfType<State>().isEmpty())

                val xborderPaymentStates = tx.outputsOfType<State>()
                val instructionStates = tx.outputsOfType<Instruction.State>()

                "The cross-border payment output state must be single" using (xborderPaymentStates.size == 1)
                "The instruction output state must be single" using (instructionStates.size == 1)

                val xborderPaymentState = xborderPaymentStates.first()
                val instructionState = instructionStates.first()

                "The cross-border payment output state's status must be queued" using (xborderPaymentState.status == Status.QUEUED)
                "The instruction output state's status must be proposed" using (instructionState.status == Instruction.Status.PROPOSED)

                val cashStates = tx.outputsOfType<Cash.State>()

                "The cash states must be empty" using (cashStates.isEmpty())
            }

        }

        // TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }

    interface Command : CommandData {
        class Pay: TypeOnlyCommandData(), Command
        class Queue: TypeOnlyCommandData(), Command
        class Cancel : TypeOnlyCommandData(), Command
    }

    data class State(
        val txId: String,
        val senderBank: AbstractParty,
        val receiverBank: AbstractParty,
        val sendingAmount: Amount<Currency>,
        val purpose: Purpose,
        val status: Status,
        val valueDate: Instant = Instant.now(),
        val issueDate: Instant = Instant.now(),
        val remark: String?,
        val embedLinearID: UniqueIdentifier? = null,
        override val linearId: UniqueIdentifier = UniqueIdentifier()
    ) : LinearState, SettleableAgreement {
        override fun nextSettledState() = SettleableAgreement.SettleTransition(
            output = copy(status = Status.SETTLED),
            contractClassName = XBORDER_PAYMENT_CONTRACT_ID,
            command = Command(
                Command.Pay(),
                listOf(senderBank.owningKey, receiverBank.owningKey)
            )
        )

        @get:JsonIgnore
        override val participants: List<AbstractParty> get() = listOf(senderBank, receiverBank)
    }
}

