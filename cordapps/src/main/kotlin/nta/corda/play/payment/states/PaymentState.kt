package nta.corda.play.payment.states

import com.fasterxml.jackson.annotation.JsonIgnore
import net.corda.core.contracts.Amount
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.QueryableState
import nta.corda.play.payment.contracts.PaymentContract
import nta.corda.play.payment.models.Purpose
import java.time.Instant
import java.util.*

@BelongsToContract(PaymentContract::class)
data class PaymentState(
        val txId: String,
        val sender: AbstractParty,
        val receiver: AbstractParty,
        val operator: Party? = null,
        val amount: Amount<Currency>,
        val status: PaymentContract.Status,
        val purpose: Purpose,
        val effectiveDate: Instant = Instant.now(),
        val issueDate: Instant = Instant.now(),
        val remark: String? = null,
        override val linearId: UniqueIdentifier = UniqueIdentifier()
) : LinearState {

//    override fun generateMappedObject(schema: net.corda.core.schemas.MappedSchema): net.corda.core.schemas.PersistentState {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun supportedSchemas(): kotlin.collections.Iterable<net.corda.core.schemas.MappedSchema> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }

    @get:JsonIgnore
    override val participants: List<AbstractParty> get() = listOf(sender, receiver)
}