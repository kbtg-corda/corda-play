package nta.corda.play.common

import net.corda.core.identity.CordaX500Name
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun txId(prefix: String, name: CordaX500Name): String {
    val current = LocalDateTime.now()
    val formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS")
    val formatted = current.format(formatter)
    return "$prefix${name.organisation}$formatted"
}