package nta.corda.play.common

import net.corda.core.contracts.Amount
import net.corda.finance.AMOUNT
import java.util.*

@JvmField
val THB: Currency = Currency.getInstance("THB")
val Int.THB: Amount<Currency> get() = THB(this)

fun THB(amount: Int): Amount<Currency> = AMOUNT(amount, THB)