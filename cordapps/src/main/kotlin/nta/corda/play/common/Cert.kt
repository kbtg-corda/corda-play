package nta.corda.play.common

import net.corda.nodeapi.internal.crypto.X509Utilities
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import org.bouncycastle.asn1.x509.BasicConstraints
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.cert.X509v3CertificateBuilder
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder
import org.bouncycastle.crypto.params.AsymmetricKeyParameter
import org.bouncycastle.crypto.util.PrivateKeyFactory
import org.bouncycastle.jce.X509KeyUsage
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.bouncycastle.operator.ContentSigner
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import org.bouncycastle.pkcs.PKCS10CertificationRequest
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileInputStream
import java.math.BigInteger
import java.nio.file.Paths
import java.security.KeyPair
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.*


//@Throws(Exception::class)
//fun getServerCertificate(): X509Certificate? {
//    val certificateFactory = CertificateFactory.getInstance("X.509", "BC")
//    return certificateFactory.generateCertificate(
//            FileInputStream(File("C:/Users/varun/Desktop/cert/CA/caroot.cer"))) as X509Certificate
//}
//
//fun signCsr(id: String, request: PKCS10CertificationRequest, keypair: KeyPair, cert: X509Certificate): X509Certificate {
//    val pkInfo: SubjectPublicKeyInfo = request.subjectPublicKeyInfo
//    val converter = JcaPEMKeyConverter()
//    val pubKey = converter.getPublicKey(pkInfo)
//    val certificateBuilder: X509v3CertificateBuilder = JcaX509v3CertificateBuilder(cert, BigInteger("1"),  // serial
//            Date(System.currentTimeMillis()),
//            Date(System.currentTimeMillis() + 30L * 365L * 24L * 60L * 60L * 1000L),
//            request.subject, pubKey
//    ).addExtension(ASN1ObjectIdentifier("2.5.29.19"), false, BasicConstraints(false) // true if it is allowed to sign other certs
//    ).addExtension(ASN1ObjectIdentifier("2.5.29.15"), true, X509KeyUsage(X509KeyUsage.digitalSignature
//            or X509KeyUsage.nonRepudiation or X509KeyUsage.keyEncipherment or X509KeyUsage.dataEncipherment))
//
//    val asymmetricKeyParameter: AsymmetricKeyParameter = PrivateKeyFactory.createKey(keypair.getPrivate().getEncoded())
//    // ContentSigner sigGen = new BcRSAContentSignerBuilder(sigAlgId,
//    // digAlgId).build(asymmetricKeyParameter);
//    // ContentSigner sigGen = new BcRSAContentSignerBuilder(sigAlgId,
//// digAlgId).build(asymmetricKeyParameter);
//    val sigGen: ContentSigner = JcaContentSignerBuilder("SHA1withRSA").build(keypair.getPrivate())
//
//    val x509CertificateHolder = certificateBuilder.build(sigGen)
//    val eeX509CertificateStructure = x509CertificateHolder.toASN1Structure()
//    // Read Certificate
//    // Read Certificate
//    val certificateFactory = CertificateFactory.getInstance("X.509", "BC")
//    val is1 = ByteArrayInputStream(eeX509CertificateStructure.encoded)
//    val signedCertificate = certificateFactory.generateCertificate(is1) as X509Certificate
//
////    FileSystemUtility.saveCertificate(signedCertificate.encoded, cert.encoded, id)
//
//    val path = Paths.get("").toAbsolutePath()
//    X509Utilities.saveCertificateAsPEMFile(signedCertificate, path)
//
//    return signedCertificate
//}