package nta.corda.play.common

import net.corda.core.identity.CordaX500Name

val operatorX500Name =  CordaX500Name.parse(name = "O=BOT, L=BKK, C=TH, CN=Play")