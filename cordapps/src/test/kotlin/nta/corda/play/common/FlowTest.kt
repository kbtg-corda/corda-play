package nta.corda.play.common

import net.corda.core.contracts.*
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.OpaqueBytes
import net.corda.core.utilities.getOrThrow
import net.corda.core.utilities.minutes
import net.corda.finance.contracts.asset.Cash
import net.corda.finance.workflows.getCashBalances
import net.corda.testing.internal.chooseIdentity
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.assertAll
import java.security.PublicKey
import java.util.*

val StartedMockNode.party: Party get() = this.info.chooseIdentity()
val StartedMockNode.name: CordaX500Name get() = this.party.name

fun StartedMockNode.asIssuer() = PartyAndReference(this.party, OpaqueBytes.of(0))

fun StartedMockNode.anonymise() = transaction {
    services.keyManagementService.freshKeyAndCert(info.legalIdentitiesAndCerts.first(), true).party.anonymise()
}

/**
 * Automatically select contract attachments, set time window and select relevant keys to sign the transaction.
 *
 * No need to specify [myKeys]; it only exists for old code.
 */
fun ServiceHub.signMyTransaction(tx: TransactionBuilder, myKeys: Iterable<PublicKey>? = null, setTimeWindow: Boolean = true): SignedTransaction {
//    tx.fillContractAttachments(this)

    if (setTimeWindow) {
        tx.setTimeWindow(this.clock.instant(), 5.minutes)
    }

    val myRequiredKeys = myKeys ?: tx.commands().flatMap { this.keyManagementService.filterMyKeys(it.signers) }.toSet()

    return this.signInitialTransaction(tx, myRequiredKeys)
}

fun StartedMockNode.signAndRecord(tx: TransactionBuilder, participants: Set<StartedMockNode> = emptySet()): SignedTransaction {
    val stx = this.transaction { services.signMyTransaction(tx, setTimeWindow = false) }
    (setOf(this) + participants).forEach { n ->
        n.transaction { n.services.recordTransactions(stx) }
    }
    return stx
}

fun StartedMockNode.isAnonymousMe(party: AbstractParty) =
        services.myInfo.isLegalIdentity(services.identityService.requireWellKnownPartyFromAnonymous(party))

abstract class FlowTest {
    protected lateinit var network: MockNetwork

    /**
     * Use this instead of startFlow so that you don't need to call runNetwork explicitly, and so that error from the
     * flow will be thrown.
     */
    fun <T> StartedMockNode.runFlow(logic: FlowLogic<T>): T {
        val future = this.startFlow(logic)
        // Try not to call runNetwork without arguments, since async operations may not necessarily finish without
        // starting some other flow
        while (!future.isDone) {
            network.runNetwork(20)
        }
        network.runNetwork(100)
        return future.getOrThrow()
    }

    @JvmName("pledgeRemoveIssuer")
    fun StartedMockNode.pledge(pledgee: StartedMockNode, amount: Amount<Issued<Currency>>, vararg amounts: Amount<Issued<Currency>>) =
            (amounts.toList() + amount).forEach { pledge(pledgee, it.withoutIssuer()) }

    fun StartedMockNode.pledge(pledgee: StartedMockNode, amnt: Amount<Currency>): Cash.State {
        val anonymousPledgee = pledgee.transaction {
            pledgee.services.keyManagementService.freshKeyAndCert(pledgee.services.myInfo.legalIdentitiesAndCerts.first(), false)
                    .party.anonymise()
        }

        val tx = TransactionBuilder(network.defaultNotaryIdentity)
        Cash().generateIssue(tx, Amount(amnt.quantity, Issued(this.asIssuer(), amnt.token)), anonymousPledgee, tx.notary!!)
        val stx = this.signAndRecord(tx, setOf(pledgee))
        return stx.coreTransaction.outputsOfType<Cash.State>().single()
    }

    /**
     * Extended database transaction scope provider that injects the [StartedMockNode] as receiver.
     *
     * This exposes [StartedMockNode] as [this], and avoids typos such as:
     *  ```
     *  bankA.transaction {
     *      bankB.services.vaultService.queryBy(T::class.java)
     *  }
     *  ```
     */
    fun <T> StartedMockNode.transactionEx(block: StartedMockNode.() -> T) = transaction { block(this) }

    /**
     * Asserts that cash balances of one or more [Currency] are as expected.
     *
     * Note that unspecified currencies would not be checked.
     */
    fun StartedMockNode.assertBalanceEquals(vararg expectedBalances: Amount<Currency>) {
        val actual = transactionEx { services.getCashBalances() }
        assertAll(*expectedBalances.map { {
            val actualBalance = actual[it.token] ?: Amount.zero(it.token)
            Assertions.assertEquals(it, actualBalance)
        } }.toTypedArray())
    }

    /**
     * Queries ONE state for assertion by type.
     *
     * If the result contains multiple states, it fails.
     */
    inline fun <reified T : ContractState> StartedMockNode.queryAndAssert(crossinline block: (T) -> Unit) {
        val states = transactionEx { services.vaultService.queryBy(T::class.java).states }
        Assertions.assertEquals(1, states.size)
        block.invoke(states.single().state.data)
    }

    /**
     * Queries states by type and assert that no state of such type exists.
     *
     * This is particularly useful when three parties are involved in a workflow, and not all states
     * are visible to all parties.
     */
    inline fun <reified T : ContractState> StartedMockNode.queryNone() {
        val states = transactionEx { services.vaultService.queryBy(T::class.java).states }
        Assertions.assertEquals(listOf<T>(), states)
    }
}