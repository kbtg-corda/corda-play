package nta.corda.play.common

import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.loggerFor
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkNotarySpec
import net.corda.testing.node.StartedMockNode
import java.util.*

data class ReusableNetworkConfig(
        val customCordapps: Set<String>,
        val notaries: Set<MockNetworkNotarySpec> = setOf(
                MockNetworkNotarySpec(CordaX500Name("Notary", "Bangkok", "TH"), validating = false)
        ),
        val nodes: Set<CordaX500Name>,
        val threadPerNode: Boolean = false
)

data class ReusableNetwork(
        val network: MockNetwork,
        val nodes: Map<CordaX500Name, StartedMockNode>
)

class ReusableNetworkDSL internal constructor(private val resolveNode: ((CordaX500Name) -> StartedMockNode)?) {
    fun node(legalName: CordaX500Name, accept: (StartedMockNode) -> Unit): CordaX500Name {
        if (resolveNode != null) {
            accept(resolveNode.invoke(legalName))
        }
        return legalName
    }
}

object ReusableNetworkHolder {
    private val log = loggerFor<ReusableNetworkHolder>()

    private var instance: ReusableNetwork? = null
    private var instanceConfig: ReusableNetworkConfig? = null
    private var instanceSnapshot: Map<StartedMockNode, String>? = null

    private var shutdownHook: Boolean = false

    fun acquire(reusable: Boolean = true, block: ReusableNetworkDSL.() -> ReusableNetworkConfig): ReusableNetwork {
        // Build the config from the block
        val instance = acquire(
                ReusableNetworkDSL(null).block(),
                reusable
        )
        // Set the variables using the block
        ReusableNetworkDSL { instance.nodes[it]!! }.block()
        return instance
    }

    @Synchronized
    fun acquire(config: ReusableNetworkConfig, reusable: Boolean = true): ReusableNetwork {
        if (!shutdownHook) {
            Runtime.getRuntime().addShutdownHook(Thread {
                destroyInstance()
            })
            shutdownHook = true
        }

        if (instanceConfig == config) {
            log.info("Providing ReusableNetwork by restoring from snapshot")
            loadSnapshot(instanceSnapshot!!)
        } else {
            log.info("Providing ReusableNetwork by recreation")
            destroyInstance()
            createInstance(config, reusable = reusable)
        }

        return instance!!
    }

    fun release() {}

    @Synchronized
    private fun createSnapshot(instance: ReusableNetwork): Map<StartedMockNode, String> {
        return instance.nodes.values.associate { node ->
            val id = UUID.randomUUID().toString()
            node.transaction {
                node.services.jdbcSession().createStatement().use {
                    it.execute("SCRIPT TO 'memFS:test-node-$id.sql'")
                }
            }
            node to id
        }
    }

    @Synchronized
    private fun loadSnapshot(snapshot: Map<StartedMockNode, String>) {
        for ((node, id) in snapshot) {
            node.transaction {
                val session = node.services.jdbcSession()
                session.createStatement().use { it.execute("DROP ALL OBJECTS") }
                session.createStatement().use { it.execute("RUNSCRIPT FROM 'memFS:test-node-$id.sql'") }
            }
        }
    }

    @Synchronized
    private fun createInstance(config: ReusableNetworkConfig, reusable: Boolean) {
        val network = MockNetwork(
                cordappPackages = config.customCordapps.toList(),
                notarySpecs = config.notaries.toList(),
                threadPerNode = config.threadPerNode
        )
        val nodes = config.nodes.associate { it to network.createNode(it) }
        if (!config.threadPerNode) {
            // runNetwork doesn't work when threadPerNode is used
            network.runNetwork()
        }

        instance =
                ReusableNetwork(network = network, nodes = nodes)
        instanceConfig = if (reusable) config else null
        instanceSnapshot =
                createSnapshot(instance!!)
    }

    @Synchronized
    private fun destroyInstance() {
        instance?.network?.stopNodes()
        instance = null
        instanceConfig = null
        instanceSnapshot = null
    }
}
