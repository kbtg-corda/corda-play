package nta.corda.play.payment.flows

import net.corda.core.identity.CordaX500Name
import net.corda.testing.node.StartedMockNode
import nta.corda.play.common.FlowTest
import nta.corda.play.common.ReusableNetworkConfig
import nta.corda.play.common.ReusableNetworkHolder
import nta.corda.play.common.operatorX500Name
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import java.time.Instant

abstract class PaymentFlowTest : FlowTest() {
    lateinit var centralBank: StartedMockNode
    lateinit var kasi: StartedMockNode
    lateinit var bbl: StartedMockNode

    @BeforeEach
    fun setup() {
        val instance = ReusableNetworkHolder.acquire {
            ReusableNetworkConfig(
                    customCordapps = setOf(
                            "net.corda.finance",
                            "nta.corda.play.common",
                            "nta.corda.play.payment"
                    ),
                    nodes = setOf(
                            node(operatorX500Name) { centralBank = it },
                            node(CordaX500Name("BKKBTHBK", "BKK", "TH")) { bbl = it },
                            node(CordaX500Name("KASITHBK", "BKK", "TH")) { kasi = it }
                    )
            )
        }
        network = instance.network
    }

    @AfterEach
    fun tearDown() {
        ReusableNetworkHolder.release()
    }
}