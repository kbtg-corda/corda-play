package nta.corda.play.payment.flows

import com.google.common.collect.ImmutableList
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowException
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.finance.workflows.getCashBalance
import nta.corda.play.common.THB
import nta.corda.play.payment.contracts.PaymentContract
import nta.corda.play.payment.models.PaymentRequest
import nta.corda.play.payment.models.Purpose
import nta.corda.play.payment.states.PaymentState
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class MakePaymentTest : PaymentFlowTest() {
    @Disabled
    @Test
    fun `Make payment with sufficient cash (THB)`() {
        centralBank.pledge(kasi, 5000000.THB)

        val request = PaymentRequest(
            receiver =  bbl.info.legalIdentities.first(),
            amount = 1000000.THB,
            effectiveDate = Instant.now(),
            issueDate = Instant.now(),
            purpose = Purpose.GOODS,
            remark = "1234"
        )

        val payment = kasi.runFlow(MakePayment.Sender(request))
//        val stx = kasi.transaction {
//            kasi.services.validatedTransactions.getTransaction(payment.txhash)!!
//        }
//        var linearId = UniqueIdentifier()
//
//        kasi.transaction {
//            val paymentState = stx.tx.outputsOfType<PaymentState>().first()
//            linearId = paymentState.linearId
//
//            assertEquals(PaymentContract.Status.SETTLED, paymentState.status)
//            assertEquals(4000000.THB, kasi.services.getCashBalance(THB))
//        }
//
//        bbl.transaction {
//            val paymentStateNref = getByLinearId(linearId)
//
//            assertEquals(PaymentContract.Status.SETTLED, paymentStateNref!!.state.data.status)
//            assertEquals(1000000.THB, bbl.services.getCashBalance(THB))
//        }
    }

    private fun getByLinearId(linearId: UniqueIdentifier): StateAndRef<PaymentState>? {
        val queryCriteria = QueryCriteria.LinearStateQueryCriteria(
                null,
                ImmutableList.of(linearId),
                Vault.StateStatus.UNCONSUMED, null)

        val paymentStateNref = bbl
                .services
                .vaultService
                .queryBy<PaymentState>(queryCriteria)
                .states
                .singleOrNull()
        return paymentStateNref
    }

    @Disabled
    @Test
    fun `Make payment with insufficient cash (THB)`() {
        centralBank.pledge(kasi, 1000000.THB)

        val request = PaymentRequest(
                receiver =  bbl.info.legalIdentities.first(),
                amount = 2000000.THB,
                effectiveDate = Instant.now(),
                issueDate = Instant.now(),
                purpose = Purpose.GOODS,
                remark = "1234"
        )

        val exc = assertFailsWith<FlowException> {
            kasi.runFlow(MakePayment.Sender(request))
        }

        assertEquals("Insufficient cash (THB) to makes payment", exc.message)
    }
}