#!/bin/bash

# echo $sec >>$CONFPATH

cat >>$CONFPATH <<-EOM
security {
    authService {
        dataSource {
            type=INMEMORY
            users=[
                {
                    password=$PASS
                    permissions=[
                        $PERM
                    ]
                    username=$USER
                }
            ]
        }
    }
}
EOM
