# Inthanon Cordapps

This repository contains all the Cordapps for Inthanon.

## Prerequisites

You will need the following installed on your machine before you can start:

* [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
  (8u171) installed and available on your path.
* Latest version of [IntelliJ IDEA](https://www.jetbrains.com/idea/download/)
  (note the community edition is free).
* Git

More information is available
[in Corda documentation](https://docs.corda.net/getting-set-up.html).

## Run local Corda networks

There are three local networks. For Phase 3 development, you mainly need only
the corridor network.

```sh
# For the corridor network:
./gradlew corridor-network:deployNodes
./corridor-network/build/nodes/runnodes

# For the THB Inthanon network:
./gradlew :deployNodes
./build/nodes/runnodes

# For the HKD Lion Rock network:
./gradlew hkd-network:deployNodes
./hkd-network/build/nodes/runnodes
```

The `deployNodes` command will build the cordapps and create the Corda node
installations under `build/nodes`. The `runnodes` command will start the Corda
nodes using a script.

You can use the [Corda shell](https://docs.corda.net/getting-set-up.html) to run
flows directly. Alternatively, you can start the API servers. See
[Inthanon API README](../inthanon-api/README.md).
## Corda development

During development, you usually will write flow tests to test your flows. See
existing test cases for an example.

Usually you will run test cases from IntelliJ. Corda flow tests require special
configuration. See https://stackoverflow.com/q/47411363 for more details. Note
that `quasar.jar` is available from `lib/quasar.jar` in this repository.

If you insist on running tests from gradle, you can use the following command:

```sh
# To run the tests
./gradlew --continue test -PrunTests=true

# To open all test reports (Mac only)
open */build/reports/tests/test/index.html
```
