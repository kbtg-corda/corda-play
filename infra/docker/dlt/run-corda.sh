#!/bin/sh

# If variable not present use default values
home="${CORDA_HOME:=/opt/corda}"
javaOptions="${JAVA_OPTIONS:=-Xmx512m}"

# export CORDA_HOME JAVA_OPTIONS

cd ${home}
java ${javaOptions} -jar ${home}/corda-webserver.jar 2>&1 &
java ${javaOptions} -jar ${home}/corda.jar 2>&1
